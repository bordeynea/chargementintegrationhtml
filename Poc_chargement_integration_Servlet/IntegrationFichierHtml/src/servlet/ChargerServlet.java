/**
 * 
 */
package servlet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Classe permettant de lire et de charger un fichier html
 *
 * @author Administrateur Date : 22 avr. 2021
 */
public class ChargerServlet extends HttpServlet {

	private static final long  serialVersionUID = -3831267582914808048L;

	/**
	 * constante des ligne du fichier html
	 */
	public static final String LISTLIGNEHTML    = "ligne";

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		String contenuHtml = "";

		//prendre le fichier � sa racine 
		final String findfile = "C:/test/test.html";

		//creation d'un FileReader avec le findfile
		final File file = new File(findfile);
		//on rajoute StandardCharsets.UTF_8 
		try (final Scanner scanner = new Scanner(file, StandardCharsets.UTF_8)) {

			//on boucle si non null
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				System.out.println(line);
				contenuHtml += line;
			}
		}

		//on envoie la liste en requete
		request.setAttribute(LISTLIGNEHTML, contenuHtml);

		//on forward sur l'index
		request.getRequestDispatcher("/jsp/index.jsp").forward(request, response);
	}
}
