Pour charger un fichier HTML et l'int�grer dans une jsp, on va :

Dans l'Action.java :

1. cr�er une action afin de r�cup�rer le fichier gr�ce � un FileReader que l'on place dans un scanner

2. chaque ligne du fichier est concat�n� dans une cha�ne de caract�res

3. on place cette cha�ne de caract�res en attribut de la requ�te

Dans la jsp :

4. il n'y a plus qu'� afficher le contenu du fichier � l'aide d'un �l�ment de Langage par exemple (attention, bean:write affichera les balises)
