/**
 * 
 */
package presentation.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Classe repr�sentant l'action de chargement du contenu d'un fichier HTML
 *
 * @author NathanB Date : 22 avr. 2021
 */
public class ChargerAction extends Action {

	/**
	 * liste des lignes (String) du fichier HTML
	 */
	public static final String CONTENU_HTML = "contenu_html";

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		String contenu = "";
		// on charge le fichier
		try (final Scanner scanner = new Scanner(new File("C://test/test.html"),StandardCharsets.UTF_8)) {
			// tant qu'il y a une prochaine ligne...
			while (scanner.hasNext()) {
				// ... on l'ajoute au contenu du fichier 
				contenu += scanner.nextLine();
			}
		} catch (final FileNotFoundException f) {
			f.printStackTrace();
		}
		// on met en attribut de la requ�te le contenu
		request.setAttribute("contenu_html", contenu);
		// on propage la requ�te vers la jsp
		return mapping.findForward("success");
	}

}
